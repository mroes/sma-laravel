<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Niek Hoek',
            'email' => 'niekhoek@mailinator.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'Dennes Slagmolen',
            'email' => 'dennes@mailinator.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'Renee Henstra',
            'email' => 'renee.henstra@mailinator.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'Jurren Buitink',
            'email' => 'renee.henstra@mailinator.com',
            'password' => Hash::make('password'),
        ]);
    }
}
