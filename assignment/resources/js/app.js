require('./bootstrap');

$( document ).ready(function() {
    function passwordsMatches(){
        const pass1 = $('#password').val();
        const pass2 = $('#password-verify').val();
        return pass2 === pass1;
    }

    function showError(error){
        const $error = $('.error');
        $error.html(error);
        $error.removeClass('hidden');
        setTimeout(function(){
            hideError();
        }, 3000)
    }

    function hideError(){
        $('.error').addClass('hidden');
    }
    function createMessageFormSubmitHandler(){
        $('#btn-send').on('click', function(e){

            e.preventDefault();
            /**
             * This is also checked in the back end.
             */
            if(!passwordsMatches()){
                showError('passwords dont match');
                return false;
            }
            const data = {
                password : $('#password').val(),
                password_verify : $('#password-verify').val(),
                message_content : $('#message-content').val(),
                colleague_email : $('#colleague-email').val(),
                _token : $('input[name="_token"]').val()
            }

            $.ajax({
                type : 'POST',
                url : 'store',
                data : data,
                success : function (data){
                    $('#messsage-url').html(data.url);
                },error: function (error){
                    showError(error.toString());

                }
            })
        });
    }
    createMessageFormSubmitHandler();

});

