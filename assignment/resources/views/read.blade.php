@extends('layout')

@section('content')
    <div class="row">

        <div class="col">
            <div class="well well-lg">
                <p class="text" style="word-wrap: break-word;">
                    {{$message->getContent()}}
                </p></div>

            <form action="burn/{{$message->id}}" method="post">
                @csrf
                <input type="hidden" name="message_id" value="{{$message->id}}">
                <input type="hidden" name="message_password" value="{{$message->password}}">
                <button type="submit" class="btn btn-primary">Vernietig bericht</button>
            </form>
        </div>

    </div>
@endsection
