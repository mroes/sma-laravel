@extends('layout')

@section('content')
    <div class="row">

        <div class="col">
            <div class="well well-lg">
                <p class="text" style="word-wrap: break-word;">
                    {{$message}}
                </p></div>
        </div>
    </div>
@endsection
