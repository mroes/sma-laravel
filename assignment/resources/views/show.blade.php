@extends('layout')

@section('content')
    <div class="row">

        <div class="col">
            <div class="well well-lg">
                <p class="text" style="word-wrap: break-word;">
                    {{$message->getContent()}}
                </p>
            </div>
        </div>
        <div class="col">
            <form id="decrypt-form" action="/read" method="post">
                @csrf

                <input type="hidden" id="message-id" name="message_id" value="{{$message->id}}">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="password">Voer je wachtwoord hieronder in om het bericht te ontsleutelen</label>
                            <input required id="password" name="password" type="password" class="@error('password') is-invalid @enderror">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Ontsleutel bericht</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </div>
@endsection
