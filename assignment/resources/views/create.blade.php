@extends('layout')

@section('content')
    <div class="row">

        <div class="col">
            <h1>Bericht opstellen</h1>

            <form action="store" id="create-messsage-form" method="post">
                <p class="error hidden">
                </p>
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="colleague-email">Collega</label>
                            <select id="colleague-email" name="colleague_email" class="form-control">
                                <option value="">Selecteer een collega</option>
                                @foreach ($users as $user)
                                    <option value="{{$user->email}}">{{$user->name}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="message-content">Bericht</label>

                            <textarea required id="message-content" name="message_content" class="form-control" rows="5"
                                      placeholder="Plaats hier je bericht*"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="password">Wachtwoord</label>
                            <input id="password" name="password" type="password" class="@error('password') is-invalid @enderror">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="password-verify">Wachtwoord verificatie</label>
                            <input id="password-verify" name="password_verify" type="password" class="@error('password') is-invalid @enderror">
                        </div>
                    </div>
                </div>
                <button id="btn-send" class="btn btn-primary">Versleutel bericht</button>
            </form>

        </div>
        <div class="col">
            <h3>Link naar versleuteld bericht</h3>

            <div class="message-url-wrapper">
                <p id="messsage-url">
                </p>
            </div>
        </div>

    </div>
@endsection
