<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;

trait Hashable{


    function canHashProperty(string $propertyName): bool
    {
        return in_array($propertyName, $this->hashableProperties) && isset($this->{$propertyName});
    }

    /**
     * One way encryption.
     * Some properties like password's should never be able to be redden.
     * @param $propertyName
     */
    function hashProperty($propertyName)
    {
        if($this->canHashProperty($propertyName)){
            $this->{$propertyName} = Hash::make($this->{$propertyName});
            $this->attributes[$propertyName] = $this->{$propertyName};
        }
    }
}
