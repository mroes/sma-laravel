<?php

namespace App\Models;

use App\Models\DesignPatterns\EncryptedMessage\EncryptionException;
use App\Models\DesignPatterns\EncryptedMessage\EncryptionMessageBehaviour;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model implements EncryptionMessageBehaviour
{
    use HasFactory, Hashable;
    protected $table = 'encrypted_messages';
    private ?EncryptionMessageBehaviour $state = null;

    /**
     * Message should only be able to be decrypted when $passwordValidated equals true
     * @var bool $passwordValidated
     */
    public bool $passwordValidated = false;

    public array $errors;

    /**
     * Messages that are deleted return a 404 Not found or 'burn' view
     * when they are requested in a /show or /read request
     * @var bool $deleted
     */
    public bool $deleted;

    public function __construct(array $attributes = [])
    {
        $this->changeState(new EncryptedMessageState());
        $this->errors = [];
        parent::__construct($attributes);
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setPasswordAttribute(string $value){
        $this->password = $value;
        return $this;
    }

    /**
     * Custom properties that can be hashed.
     * @var array|string[]
     */
    protected array $hashableProperties = [
        'password'
    ];

    protected $attributes = [
        'password'=> '',
        'content' => '',
        'deleted' => false
    ];

    protected $fillable = [
        'password',
        'content',
        'deleted'
    ];

    /**
     * Password will be hashed and content will be encrypted but not through casting.
     * We will let the state handle this.
     * @var string[]
     */
    protected $casts = [
        'password' => 'string',
        'content' =>  'string',
        'deleted' => 'bool'
    ];

    /**
     * Returns encrypted text when in 'encrypted' state or decrypted text when in 'decrypted' state.
     * @return string
     */
    public function getContent(): string
    {
        return $this->state->getContent();
    }

    /**
     * Fluent setter
     * Encrypts content when in 'encrypted' state or unencrypted when in 'decrypted' state.
     * @param string $content
     * @return EncryptionMessageBehaviour
     */
    public function setContent(string $content): EncryptionMessageBehaviour
    {
        $this->state->setContent($content);
        return $this;
    }

    /**
     * Can decrypt in encrypted state when Message has valid password and
     * a submitted password has been verified.
     * @see validateSubmittedPassword
     * @return $this
     */
  public  function decryptContent(): Message
    {
        $this->state->decryptContent();
        return $this;
    }

    /**
     * Encrypts the message content when called
     * @param $value
     * @return EncryptionMessageBehaviour
     */
   public function encryptContent($value): EncryptionMessageBehaviour
    {
        $this->state->encryptContent($value);
        return $this;
    }
    public function isDecrypted():bool{
        return (isset($this->state) && $this->hasState(DecryptedMessageState::class));
    }

    public function isEncrypted(): bool
    {
        return (isset($this->state) && $this->hasState( EncryptedMessageState::class));
    }

    /**
     * Returns true when message has a valid hashed password and $unEncryptedValue is not empty.
     * @param string $unEncryptedValue
     * @return bool
     */
    function canEncrypt(string $unEncryptedValue):bool{
        return $this->state->canEncrypt($unEncryptedValue);
    }

    /**
     * todo refactor single responsiblity?
     * @param EncryptionMessageBehaviour $state
     */
    public function changeState(EncryptionMessageBehaviour $state)
    {
        $this->state = $state;
        $this->state->setContext($this);
    }

    private function hasState(string $stateClass):bool{
        return (isset($this->state) && $this->state instanceof $stateClass);
    }

    public function passwordInputIsValid(string $expectedPassword): bool{
       return $this->state->passwordInputIsValid($expectedPassword);
    }

    public function validateSubmittedPassword(string $submittedPassword){
        $this->state->validateSubmittedPassword($submittedPassword);

    }

    public function setIsPasswordValidated(bool $isValidated)
    {
        $this->passwordValidated = $isValidated;
    }


    function canDecrypt(): bool
    {
        return $this->state->canDecrypt();
    }

    function hasValidPassword(): bool
    {
        return $this->state->hasValidPassword();
    }

    function addError(EncryptionException $encryptionException){
        $this->errors[] = $encryptionException;
    }

    public function getErrors():array
    {
        return $this->errors;
    }

    public function resetErrors(){
        $this->errors = [];
    }

    /**
     * Stores a message as deleted.
     * When called this message should be unable to be retrieved afterwards when processing requests like /show or /read.
     */
    public function burn(){
        $this->deleted = true;
        $this->setAttribute('deleted', true);
        $this->save();
    }
}
