<?php

namespace App\Models;

use App\Models\DesignPatterns\EncryptedMessage\EncryptionMessageBehaviour;
use App\Models\DesignPatterns\EncryptedMessage\MessageBaseState;

class DecryptedMessageState extends MessageBaseState implements EncryptionMessageBehaviour
{

    function getContent(): string
    {
        if( $this->canDecrypt()){
            $this->decryptContent();
        }
        return $this->context->content;
    }

    function setContent(string $content)
    {
        $this->context->setAttribute('content', $content);
    }

    function setContext(Message $context)
    {
        $this->context = $context;
    }

    function canDecrypt(): bool
    {
        return true;
    }
}
