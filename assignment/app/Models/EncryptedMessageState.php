<?php

namespace App\Models;

use App\Models\DesignPatterns\EncryptedMessage\EncryptionException;
use App\Models\DesignPatterns\EncryptedMessage\EncryptionMessageBehaviour;
use App\Models\DesignPatterns\EncryptedMessage\MessageBaseState;

class EncryptedMessageState extends MessageBaseState implements EncryptionMessageBehaviour
{


    function getContent(): string
    {
        return $this->context->content;
    }

    function setContent(string $content)
    {
        $this->encryptContent($content);
    }

    function setTitle()
    {

    }

    function setContext(Message $context)
    {
        $this->context = $context;
    }


    /**
     * Returns false when invalid password or content is supplied to the message.
     * @return bool
     */
    function canDecrypt(): bool
    {
        $this->context->resetErrors();
        $hasValidPassword = $this->hasValidPassword();
        $passwordVerified = $this->context->passwordValidated;
        $validContext = isset($this->context['content']) && !empty($this->context['content']);
        if (!$hasValidPassword) {
            $this->context->addError(new EncryptionException('Invalid password'));
        }
        if (!$passwordVerified) {
            $this->context->addError(new EncryptionException('Password mismatch'));
        }
        if (!$validContext) {
            $this->context->addError(new EncryptionException('Invalid content'));
        }
        return empty($this->context->getErrors());
    }

}
