<?php

namespace App\Models\DesignPatterns\EncryptedMessage;


use App\Models\DecryptedMessageState;
use App\Models\EncryptedMessageState;
use App\Models\Message;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

abstract class MessageBaseState implements EncryptionMessageBehaviour
{
    protected Message $context;

    function setContext(Message $context)
    {
        $this->context = $context;
    }


    /**
     * Decrypts the message content so that is readable by humans.
     */
    function decryptContent()
    {
        if ($this->context->isEncrypted()) {
            $this->context->setAttribute('content', Crypt::decryptString($this->context['content']));
            $this->context->changeState(new DecryptedMessageState());
        }
    }

    /**
     * todo refactor into strategy?
     * When called encrypts the message content with a bcrypt encryption.
     * @param $value
     */
    function encryptContent($value)
    {
        $this->context->setAttribute('content', Crypt::encryptString($value));
        if (!$this->context->isEncrypted()) {
            $this->context->changeState(new EncryptedMessageState());
        }
    }

    /**
     * Logic is state dependent.
     * @return bool
     */
    abstract function canDecrypt(): bool;

    function canEncrypt(?string $unEncryptedValue): bool
    {
        return (!empty($unEncryptedValue) && isset($this->context->password) && $this->hasValidPassword());
    }

    function hasValidPassword(): bool
    {
        $x = isset($this->context->password) ? Hash::info($this->context->getAttribute('password')) : null;
        return (!is_null($x['algo']));
    }

    /**
     * Verification and validation check for submitted password.
     * @param string $submittedPassword
     */
    function validateSubmittedPassword(string $submittedPassword)
    {
        $valid = $this->hasValidPassword();
        $verified = $this->passwordInputIsValid($submittedPassword);
        if ($valid && $verified) {
            $this->context->setIsPasswordValidated(true);
        }
    }

    /**
     * Checks if submitted password matches corresponding message password.
     * @param string $expected_password
     * @return bool
     */
    function passwordInputIsValid(string $expected_password): bool
    {
        $valid = false;
        if ($this->hasValidPassword()) {
            $valid = Hash::check($expected_password, $this->context->password);
        }
        return $valid;
    }
}
