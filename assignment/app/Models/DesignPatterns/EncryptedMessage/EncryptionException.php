<?php

namespace App\Models\DesignPatterns\EncryptedMessage{

    use Exception;

    /**
     * Custom exception class.
     * Nothing special here.
     * Class EncryptionException
     * @package App\Models\DesignPatterns\EncryptedMessage
     */
    class EncryptionException extends Exception {}
}
