<?php

namespace App\Models\DesignPatterns\EncryptedMessage;

/**
 * The common encryption Message interface
 * Interface EncryptionMessageBehaviour
 * @package App\Models\DesignPatterns\EncryptedMessage
 */
interface EncryptionMessageBehaviour
{
    function encryptContent($value);
    function decryptContent();
    function getContent():?string;
    function setContent(string $content);
    function canEncrypt(string $unEncryptedValue):bool;
    function canDecrypt():bool;
    function hasValidPassword():bool;
    function passwordInputIsValid(string $expected_password):bool;
    public function validateSubmittedPassword(string $submittedPassword);
}
