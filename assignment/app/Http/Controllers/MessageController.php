<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateMessage;
use App\Http\Requests\ReadMessage;
use App\Models\Message;
use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){}

    public function create(): Renderable
    {
        return view('create', [
            'users' => User::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateMessage $request
     * @return JsonResponse
     */
    public function store(CreateMessage $request)
    {
        $data = $request->hydrate();
        $message = new Message();
        $message->fill($data);
        $message->hashProperty('password');
        $message->encryptContent($data['content']);
        $message->save();
        $url = "show/{$message->id}";
        return response()->json(['url' =>  $url], 201);
    }


    /**
     * Reads a decrypted messsage
     * @param ReadMessage $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function read(ReadMessage $request)
    {

        $message = Message::find($request->input('message_id'));
        if (!!$message && $message->getAttribute('deleted') != 1) {
            $message->validateSubmittedPassword($request->input('password'));
            if ($message->canDecrypt()) {
                $message->decryptContent();
            }
            return response()->view('read', ['message' => $message], 200);
        }
        return response(404);
    }

    /**
     * Display the encrypted message.
     * (supply password to unlock)
     * @param int $id
     * @return Renderable
     */
    public function show($id): Renderable
    {
        $message = Message::find($id);
        if ($message && $message->getAttribute('deleted') != 1) {
            $response = view('show', [
                'message' => $message
            ]);
        } else {
            $response = view('burn', [
                'message' => 'Bericht is verwijderd.'
            ]);
        }
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Stores the message as deleted!
     *
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function burn(Request $request, int $id): Renderable
    {
        $message = Message::find($id);
        if (!!$message && $message->password === $request->input('message_password')) {
            $message->burn();
        }
        return view('burn', ['message' => 'Bericht is verwijderd']);
    }
}
