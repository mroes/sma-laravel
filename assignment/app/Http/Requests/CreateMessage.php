<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message_content' => 'required|string',
            'password' => 'nullable|required_with:password_verify|string',
            'password_verify' => 'required',
        ];
    }

    /**
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ( $this->password_verify !== $this->password ) {
                $validator->errors()->add('password_verify', 'Passwords do not match.');
            }
        });
        return;
    }

    public function hydrate(){
        $data = $this->all();
        return [
            'content' => $data['message_content'],
            'password' => $data['password']
        ];
    }
}
