<?php

namespace App\Http\Requests;

use App\Models\Message;
use Illuminate\Foundation\Http\FormRequest;

class ReadMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|string',
            'message_id' => 'required',
        ];
    }


    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        // checks user current password
        // before making changes
        $validator->after(function ($validator) {
            $x = $this->password;
            $message = Message::find($this->message_id);
            $xx = $message->password;
            if(!!$message){

                $message->validateSubmittedPassword($this->password);
                if(!$message->canDecrypt()){
                    $validator->errors()->add('password', 'Passwords do not match.');
                }
            }
        });
        return;
    }
}
