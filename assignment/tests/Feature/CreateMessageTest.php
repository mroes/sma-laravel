<?php
declare(strict_types=1);

namespace Tests\Feature;


use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateMessageTest extends TestCase
{
    public function testCreateMessageFormIsShown()
    {
        $response = $this->get(action('MessageController@create'));
        $response->assertOk();
        $response->assertSee(config('app.name'));
        $response->assertSee('Plaats hier je bericht');
        $response->assertSee('Versleutel bericht');
    }

    public function testCanStoreEncryptedMessage(){
        $response = $this->postJson('/store',
            [
                '_token' =>     csrf_token(),
                'message_content' => 'test bericht',
                'password' => 'wachtwoord',
                'password_verify' => 'wachtwoord',
                'colleague_email' => 'sample@sample.nl',
            ]);

        $response->assertStatus(201)
            ->assertJson(fn (AssertableJson $json) =>
            $json->has('url')
        );;
    }


    public function testCanShowEnncryptedMessage(){
        $response = $this->postJson('/store',
            [
                '_token' =>     csrf_token(),
                'message_content' => 'test bericht',
                'password' => 'wachtwoord',
                'password_verify' => 'wachtwoord',
                'colleague_email' => 'sample@sample.nl',
            ]);
        $response->assertStatus(201);
        $newResponse = $this->get($response->json('url'));
        $newResponse->assertOk();
        $newResponse->assertSee(config('app.name'));
    }

}
