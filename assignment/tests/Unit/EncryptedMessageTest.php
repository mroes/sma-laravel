<?php

namespace Tests\Unit;

use App\Models\Message;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class EncryptedMessageTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function canInstantiateEncryptedMessageTest()
    {
        $this->assertInstanceOf(Message::class, Message::factory()->make());
    }

    /** @test */
    public function defaultCreatedMessageHasDecryptedState(){
        $message = Message::factory()->make();
        $this->assertTrue($message->isEncrypted());
    }
    /**
     * @test
     */
    public function canEncryptMessageContentTest(){
        $message = Message::factory()->make();
        $message->password = 'random123';
        $message->hashProperty('password');
        $this->assertTrue( $message->canEncrypt('random123'));
        $message->setAttribute('content','test');
        $this->assertNotSame('test', $message->encryptContent('test')->getContent());
    }

    /** @test */
    public function encryptedMessageHasInitialEncryptedMessageStateTest(){

        $message = Message::factory()->make();
        $message->setContent('test');
        $this->assertTrue(
            Message::factory()->make()
            ->isEncrypted());
    }

    /**
     * @test
     */
    public function canDecryptMessageContentTest(){
        $message = Message::factory()->make();
        $message->password = 'kaas';
        $message->encryptContent('test');
        $this->assertNotSame('test', $message->getContent());
        $this->assertTrue($message->isEnCrypted());
        $this->assertEquals('test', $message->decryptContent()->getContent());
    }

    /** @test */
    public function messageHasPasswordTest(){
        $message = Message::factory()->make();
        $message->setAttribute('password', 'kaas');
        $this->assertNotNull($message->password);
        $this->assertEquals('kaas', $message->password);
        $message->hashProperty('password');
        $this->assertNotEquals('kaas',$message->password );
        $this->assertNotEmpty($message->password );
    }

    /** @test */
    public function messageHashedPasswordUsesBcrypAlgorithmWithExpectedCost(){
        $message = Message::factory()->make();
        $message->setAttribute('password', '123Mo3!LiJkTeRadenWachtw00rd');
        $message->hashProperty('password');
        $passwordHashInfo = Hash::info($message->password);
        $this->assertIsArray($passwordHashInfo);
        $this->assertArrayHasKey('algoName',$passwordHashInfo);
        $this->assertArrayHasKey('options',$passwordHashInfo);
        $this->assertIsArray($passwordHashInfo['options'] );
        $this->assertArrayHasKey('cost',$passwordHashInfo['options']);
        $this->assertEquals('bcrypt', $passwordHashInfo['algoName'] );
        $this->assertEquals(10, $passwordHashInfo['options']['cost'] );
        $this->assertRegExp('/^\$2[aby]?\$[\d]+\$[.\/A-Za-z0-9]{53}$/', $message->password);
    }

    /** @test */
    public function canVerifyPasswordTest(){
        $message = Message::factory()->make();
        $message->setAttribute('password', '123Mo3!LiJkTeRadenWachtw00rd');
        $message->hashProperty('password');
        $this->assertNotEquals('123Mo3!LiJkTeRadenWachtw00rd', $message->password );
        $this->assertTrue($message->passwordInputIsValid('123Mo3!LiJkTeRadenWachtw00rd'));
    }

    /** @test */
    public function canStoreEncryptedMessage(){
        $message = Message::factory()->make();
        $message->password = '123Mo3!LiJkTeRadenWachtw00rd';
        $message->hashProperty('password');
        $message->setContent('random123');
        $this->assertTrue( $message->canEncrypt('random123'));
        $message->save();
        $this->assertNotNull($message->id);
        $message->delete();
    }

    /** @test */
    public function canDecryptStoredEncryptedMessage(){
        $message = Message::factory()->make();
        $message->password = '123Mo3!LiJkTeRadenWachtw00rd';
        $message->hashProperty('password');
        $message->setContent('random123');
        $this->assertTrue( $message->canEncrypt('random123'));
        $message->save();
        $this->assertNotNull($message->id);
        $storedMessage = Message::find($message->id);
        $this->assertNotNull($storedMessage);
        $this->assertFalse($message->canDecrypt());
        $message->validateSubmittedPassword('123Mo3!LiJkTeRadenWachtw00rd');
        $this->assertTrue($message->canDecrypt());
        $message->decryptContent();
        $this->assertEquals('random123', $message->getContent());
        $message->delete();

    }
}
